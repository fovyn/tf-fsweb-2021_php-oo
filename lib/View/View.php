<?php


namespace Lib\View;

/**
 * Class View représente l'information d'une vue html
 * @package Lib\View
 */
class View
{
    /** @var string $name */
    private string $name;
    /** @var array $vars */
    private array $vars;

    public function __construct($name, $vars)
    {
        $this->name = $name;
        $this->vars = $vars;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getVars() {
        return $this->vars;
    }
}