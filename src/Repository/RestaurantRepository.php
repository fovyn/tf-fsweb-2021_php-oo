<?php


namespace App\Repository;


use App\Model\Adresse;
use App\Model\Restaurant;

class RestaurantRepository
{
    /** @var \PDO $pdo */
    private $pdo;

    public function __construct() {
        $this->pdo = new \PDO("mysql:host=localhost;port=3306;dbname=tffsweb2021-php", "root", "root");
    }

    public function getAll() {
        /** @var \PDOStatement $stmt */
        $stmt = $this->pdo->prepare("SELECT * FROM restaurant");

        $stmt->execute();

        /** @var array $res */
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $data = [];

        foreach ($res as $resto) {
            $restaurant = new Restaurant();
            $restaurant->setId($resto["id"]);
            $restaurant->setName($resto["name"]);

            $address = new Adresse();
            $address->setRue($resto["adresseRue"]);
            $address->setVille($resto["adresseVille"]);
            $address->setCp($resto["adresseCp"]);
            $address->setNum($resto["adresseNum"]);

            $restaurant->setAdresse($address);

            $data[] = $restaurant;
        }

        return $data;
    }
}