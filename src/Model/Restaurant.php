<?php


namespace App\Model;


class Restaurant
{
    /** @var int îd */
    private $id;
    /** @var string $name */
    private $name;
    /** @var Adresse $adresse */
    private $adresse;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Restaurant
     */
    public function setId(int $id): Restaurant
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Restaurant
     */
    public function setName(string $name): Restaurant
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Adresse
     */
    public function getAdresse(): Adresse
    {
        return $this->adresse;
    }

    /**
     * @param Adresse $adresse
     * @return Restaurant
     */
    public function setAdresse(Adresse $adresse): Restaurant
    {
        $this->adresse = $adresse;
        return $this;
    }


}