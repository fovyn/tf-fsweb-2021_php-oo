<?php


namespace App\Controller;



use Lib\View\View;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController
{
    /**
     * Action index du controller HomeController
     * @return View
     */
    public function index() {
        return new View("index", [
            "title" => "Hello world",
            "data" => ["Flavian", "Anne", "Cédric", "François"]
        ]);
    }
}