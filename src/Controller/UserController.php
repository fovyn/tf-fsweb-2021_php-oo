<?php


namespace App\Controller;


use Lib\View\View;

class UserController
{
    public function list() {
        return new View('list', []);
    }

    public function display($id) {
        return new View('display', ["user" => null]);
    }
}