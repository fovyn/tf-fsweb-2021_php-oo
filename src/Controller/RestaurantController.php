<?php


namespace App\Controller;


use App\Repository\RestaurantRepository;
use Lib\View\View;

class RestaurantController
{
    /** @var RestaurantRepository $restaurantRepository */
    private $restaurantRepository;

    public function __construct()
    {
        $this->restaurantRepository = new RestaurantRepository();
    }

    public function list() {
        return new View("list", [
            "restaurants" => $this->restaurantRepository->getAll()
        ]);
    }
}